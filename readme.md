# Documentation

The process to transcode a game created with The Quill to ngPAWS might have some small issues.

## Tools

- [Fuse](https://sourceforge.net/projects/fuse-emulator/) ZX Emulator
- [UnQuill](http://www.seasip.info/Unix/UnQuill/) Dissassembles a game created with The Quill
- [The Inker](https://github.com/8bat/the_inker/) Transcode tool
- [ngPAWS](https://github.com/Utodev/ngPAWS/) ngPAWS compiler

## Basic process

UnQuill supports .SNA ZX snapshot files. If you need to convert your file to .SNA format you could use Fuse emulator

- Install Fuse: 
    - on Ubuntu and derivates `sudo apt install fuse`
    - Flatplack at [flathub](https://flathub.org/apps/details/net.sf.fuse_emulator)
- Open game with Fuse
- Press F2 or got File/Save snapshot to export a .SNA snapshot

Compile UnQuill:
- Open a terminal and cd to the directory containing UnQuill code
- type `make` and wait for it to compile. If no errors, continue :)

Dissasemble game:
- open a terminal and type `./unquill -O[filename.txt] [route/snafile]`
- replace `[filename.txt]` for the name of the output file you choose and `[route/snafile]` for the route to the .sna file

Get The Inker:
- If you have git installed on your system type: `git clone git@github.com:8bat/the_inker.git`
- If you don't, [download](https://github.com/8bat/the_inker/archive/refs/heads/master.zip) a zip file from the source
- Once done, uncompress the zip file. Make sure the_inker directory is at the same directory level (./)

Game graphics:
- If the game has graphics, type on the terminal `./the_inker/script/the_inker.pl --out=graphics/ [filename.txt]` replacing `[filename.txt]` with the name you chose before. If you placed the_inker in other directory, you will need to repace the route accordangly
- If you get no errors, you can go to the next step. Otherwise check [this issue](https://github.com/8bat/the_inker/issues/6) and help debugging

Get ngPAWS:
- If you have git installed on your system, type in a terminal `git clone git@github.com:Utodev/ngPAWS.git` and run `sh ./ngPAWS/build_linux.sh`
- Download [a zip](https://github.com/Utodev/ngPAWS/archive/refs/heads/master.zip) file for the source or from [ngPAWS website](https://www.ngpaws.com/ngpaws/#Downloads) and unzip. make sure your directory structure is `./ngPAWS/dist-en`

Transcode to ngPAWS:
- Type in the terminal `./the_inker/script/the_translator.pl --out=ngpawsgame --images=graphics [filename.txt]`
- If the game has no graphics, omit `--images=graphics`

Clean up code:
- This is the most difficult part :) as you need to know the game and the code. 
- As [8bitAG](https://intfiction.org/t/thequill-to-ngpaws-if-project/55260/2) mentions, The Quill vocabulary is of 4 characters. This can be specially problematic in English with noun and verb shorten words clashing.
- Check verbs `vb_` and nouns `nn_`
- Check for verbose action. 8bitAG Example: `LIGH LAMP DROP 1 GET 1 SWAP 1 0 OK` will produce extra feedback messages in ngPAWS. You must adapt them.
- Go through the lines commented with a `TODO:`
- Fix empty lines with the text `(extra message added so the newlines below aren't included in the message above)`
- Don't forget to check some Common Errors see below

Compile for ngPAWS
- Change directory to `cd ngpawsgame`
- Copy the required files to the `ngpawsgame` directory typing `cp ../ngPAWS/dist-en/index.html ../ngPAWS/dist-en/css.css ../ngPAWS/dist-en/buzz.js ../ngPAWS/dist-en/jquery.js ./`
- If the game has graphics, type `../ngPAWS/dist-en/txtpaws -Idat code.txp` otherwise type omit `-Idat` and check for errors
- If all went well, type `../ngPAWS/dist-en/ngpc code.sce` and check for errors
- Open the main html for the game in the browser you can type `browse ./index.html`

## Common Errors
Have in mind that the process being quite straight forward need to be adapted. Some of the most common issues are:
- Some verbs (specially directions) get lost in the convertion
- Some objects get lost in the convertion (specially the ones not created at the begining of the adventure) genearting this error: Error "code.sce:xxxx:Sxxx: Semantic error: invalid object number or invalid order."
- ngPAWS uses a more complex parser. Vocabulary definition of verb and nouns is important and will clash from The Quill condacts. Some `noun noun` might need to get adapted ad `_ noun NOOUN2 noun` or similar
- Some condacts misses the `ok` or `done` in conversion.
- Rarely, some system messages gets messed giving [erroneous messages](https://gitlab.com/interactivefiction/Quann_Tulla-1984-Mike_Smith-Gary_Kelbrick/-/blob/main/ngpaws/code.txp) Check for default [system messages](https://github.com/Utodev/ngPAWS/wiki/System-messages)

